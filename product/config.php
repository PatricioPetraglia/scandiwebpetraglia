<?php
	class Database{
		private $conn;

        	public function __construct() {
			$servername = "localhost";
			$username = "id16007398_petragliap";
			$password = "nUaSM5h(~URbvW=I";
			$dbname = "id16007398_stock";
				
			$this->conn = new mysqli($servername, $username, $password, $dbname);

			if ($this->conn->connect_error) {			  
				die("Connection failed: " . $this->conn->connect_error);
			}
        	}

        	public function query($query)
        	{
            		return $this->conn->query($query);
        	}
        	public function close()
        	{
            		return $this->conn->close();
        	}			
        	public function secure($sql)
        	{
            		return $this->conn->real_escape_string($sql);
        	}		

     	}	
	
	$db = new Database();
?>