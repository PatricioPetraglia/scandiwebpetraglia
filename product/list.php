<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <div class="main-container">
        <div class="container-fluid" style="margin-top:30px">

      
            <div class="row align-items-center"> 
                
                <div class="col-6"> 
                    <div class="header"> 
                        <h1 class="header__text">Product List</h1>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-end">	
                        <button type="button" class="btn btn-primary" id='addButton' style='margin-right: 15px; font-size: 20px;'>Add</button>
                        <button type="button" class="btn btn-primary" id='deleteButton'style='margin-right: 15px; font-size: 20px;'>Mass delete</button>
                    </div> 

                </div> 
            </div> 
            <hr/>

        </div>

        <div class="container-fluid"  id="tableContainer" >
        </div>

    </div>
    <div class="footer" >
        <hr/>
        <p class="footer__paragraph">Scandiweb Test assignment</p>
    </div>
    <script>

		function loadProductsTable(){
			 $.get("getProductList.php", function(data){
				$('#tableContainer').html(data);
			});			
		}
		loadProductsTable();
		$(document).ready(function(){
			$("#deleteButton").on('click', function(e) {
				$("input:checked[type = 'checkbox']").each(function(){
					$.post($(this).attr("data-postUrl"),
		            {
						sku: $(this).val()
					},
					function(data,status, xhr){
						loadProductsTable();
					});
				});
			});

			$("#addButton").on('click', function(e) {
				window.location.href = "add.php";
			});
		});
	</script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>