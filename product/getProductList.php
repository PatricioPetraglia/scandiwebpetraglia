<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<?php
			include 'config.php';
		
			abstract class Product {
			  public $SKU;
			  public $name;
			  public $price;
			  public function __construct($SKU, $name, $price) {
				$this->SKU = $SKU;
				$this->name = $name;
				$this->price = $price; 
			  }
			  public function displayProductAttributes() {
				echo "{$this->SKU} <br> {$this->name} <br> \${$this->price} <br>"; 
			  }
			  
			  abstract public function displaySpecificAttribute();
			  abstract public function displayPostUrl();
			  
			}

			class Book extends Product {

			  public $weight;

			  function __construct($SKU, $name, $price, $weight) {
				 parent::__construct($SKU, $name, $price);
				 $this->weight = $weight;
			  }

			  public function displaySpecificAttribute() {
				echo "Weight: {$this->weight} kg. ";  
			  }
			  
			  public function displayPostUrl() {
				echo "deleteBook.php";  
			  }
			}

			class Furniture extends Product {

			  public $dimensions;

			  function __construct($SKU, $name, $price, $dimensions) {
				 parent::__construct($SKU, $name, $price);
				 $this->dimensions = $dimensions;
			  }

			  public function displaySpecificAttribute() {
				echo "Dimensions: {$this->dimensions}";  
			  }
			  public function displayPostUrl() {
				echo "deleteFurniture.php";  
			  }			  
			}

			class DvdDisk extends Product {

			  public $size;

			  function __construct($SKU, $name, $price, $dimensions) {
				 parent::__construct($SKU, $name, $price);
				 $this->dimensions = $dimensions;
			  }

			  public function displaySpecificAttribute() {
				echo "Size: {$this->dimensions} MB";  
			  }
			  public function displayPostUrl() {
				echo "deleteDvdDisk.php";  
			  }			  
			}
		
			$sql = "SELECT product_id, name, price, size FROM products INNER JOIN dvd_disks ON product_id = dvddisk_id";
			$result = $db->query($sql);

			if ($result->num_rows > 0) {
			  while($row = $result->fetch_assoc()) {
				  $products[] = new DvdDisk($row["product_id"], $row["name"], $row["price"], $row["size"]);
			  }
			} 
			
			$sql = "SELECT product_id, name, price, weight FROM products INNER JOIN books ON product_id = book_id;";
			$result = $db->query($sql);

			if ($result->num_rows > 0) {

			  while($row = $result->fetch_assoc()) {				  
				  $products[] = new Book($row["product_id"], $row["name"], $row["price"], $row["weight"]);
			  }
			} 
			
			$sql = "SELECT product_id, name, price, dimensions FROM products INNER JOIN furniture ON product_id = furniture_id";
			$result = $db->query($sql);

			if ($result->num_rows > 0) {
			  while($row = $result->fetch_assoc()) {
				  $products[] = new Furniture($row["product_id"], $row["name"], $row["price"], $row["dimensions"]);
				
			  }
			} 
			
			?>
			
			<div class="row justify-content-start">


				<?php

				if(isset($products))
				{
					foreach ($products as $prod) {

				?>
					<div class="col-2" style='margin-top: 15px; margin-bottom: 15px;'>
						<div class="item-box" >
							<div class="checkbox">
								<input type="checkbox" value="<?php echo $prod->SKU; ?>" data-postUrl="<?php $prod->displayPostUrl() ?>">
							</div>
							<div >
								<p class="item-box__paragraph">
									
									<?php
										$prod->displayProductAttributes();
										$prod->displaySpecificAttribute();
									?>                            
								</p>
							</div>
		
						</div>
					</div>
				
				
			
				  
			<?php
				}
			}
			?>
			</div>
	</body>
</html>