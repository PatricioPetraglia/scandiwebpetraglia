<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product Add</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
<body>
    <div class="main-container" >
        <div class="container-fluid" style="margin-top:30px">

        
            <div class="row align-items-center"> 
                
                <div class="col-6"> 
                    <div class="header">
                        <h1 class="header__text">Product Add</h1>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-end">
                        <button type="button" class="btn btn-primary" id='saveButton' style='margin-right: 15px; font-size: 20px;'>Save</button>
                        <button type="button" class="btn btn-primary" id='cancelButton' style='margin-right: 15px; font-size: 20px;'>Cancel</button>
                    </div> 

                </div> 
            </div> 
            <hr/>

        </div>
        <div class="container-fluid" style="margin-top:30px;">
            <form class = "add-product-form" >
                
                <div class="form-group row"style="margin-top:20px">
                    <label for="fSku" class="col-sm-2 col-form-label">SKU:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="fSku" name="fSku">
                    </div>
                </div>
                <div class="form-group row"style="margin-top:20px">
                    <label for="fName" class="col-sm-2 col-form-label">Name:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="fName"  name="fName">
                    </div>
                </div>
                <div class="form-group row" style="margin-top:20px">
                    <label for="fPrice" class="col-sm-2 col-form-label">Price:</label>
                    <div class="col-sm-2">              
                        <input type="text" class="form-control" id="fPrice"  name="fPrice">
                    </div>
                </div>
                <div class="form-group row" style="margin-top:20px">
                    <label for="fTypeSwitcher" class="col-sm-2 col-form-label">Type switcher:</label>
                    <div class="col-sm-2"> 
                        <div class="dropdown">
                            <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="fTypeSwitcher" data-bs-toggle="dropdown" aria-expanded="false">
                                Type switcher
                            </button>
                            <ul class="dropdown-menu" id="fTypeSwitcherMenu">
                                <li><a class="dropdown-item" onclick = "setAsDvdDisk()" >Dvd disk</a></li>
                                <li><a class="dropdown-item" onclick = "setAsBook()">Book</a></li>
                                <li><a class="dropdown-item" onclick = "setAsFurniture()">Furniture</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group row" id = "lSize" style="margin-top:20px; display:none">
                    <label for="fSize" class="col-sm-2 col-form-label">Size (MB):</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="fSize"  name="fSize" >
                    </div>
                </div>
                <div class="form-group row" id = "lWeight" style="margin-top:20px; display:none">
                    <label for="fWeight" class="col-sm-2 col-form-label">Weight (KG):</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="fWeight"  name="fWeight" >
                    </div>
                    
                </div>
                <div class="form-group" id = "lDimensions" style="display:none">
                    <div class="form-group row" id = "lWeight" style="margin-top:20px">
                        <label for="fHeight" class="col-sm-2 col-form-label">Height (CM):</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="fHeight"  name="fHeight" >
                        </div>
                        
                    </div>
                    <div class="form-group row" id = "lWeight" style="margin-top:20px">
                        <label for="fWidth" class="col-sm-2 col-form-label">Width (CM):</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="fWidth"  name="fWidth" >
                        </div>
                        
                    </div>
                    <div class="form-group row" id = "lWeight" style="margin-top:20px">
                        <label for="fLength" class="col-sm-2 col-form-label">Length (CM):</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="fLength"  name="fLength" >
                        </div>
                        
                    </div>
                        
                </div>
                <div class="col-sm-4">
                    <div class="message-box" id = "MessageBox"><p class="message-box__text" id="MessageBoxText"></p></div>	
                </div>
                <div class="col-sm-4">
                    <div class="error-box" id = "ErrorBox"><p class="error-box__text" id="ErrorBoxText"></p></div>	
                </div>
            

            </form>
        </div>
    </div>
    <div class="footer" >
            <hr/>
            <p class="footer__paragraph">Scandiweb Test assignment</p>
    </div>



	<script>
		class GUI {
			
			
			showInputFields() {	
			}
			
			validateTypeData(){
				
				return true;
            }

			post(url, obj){

				$.post(url,
				obj,
					
				function(data,status, xhr){
					
					window.location.href = "list.php";
						
				}).fail(
					function(xhr, status, error){
					
                        if(xhr.status == 405){

							$("#ErrorBoxText").text("Duplicated SKU, please provide a different SKU.");
							$("#ErrorBox").show();
														
						}
						
					}				
				);				
			}

		}
		
		class BookGUI extends GUI {
			
			showInputFields() {
				$("#lDimensions").hide();
				$("#lSize").hide();				
				
				$("#lWeight").show();
				
				$("#MessageBoxText").text("Please, provide weight");
				$("#MessageBox").show();				
				
			}
			validateTypeData(){
				return !isNaN($("#fWeight").val());
			}
			post()
			{
                super.post("addBook.php",{
                    sku: $("#fSku").val(),
					name: $("#fName").val(),
					price: $("#fPrice").val(),
					weight: $("#fWeight").val()
				});				
				
			}
		}
	
		class DvdDiskGUI extends GUI {

			showInputFields() {
				$("#lWeight").hide();
				$("#lDimensions").hide();
				
				$("#lSize").show();
				
				$("#MessageBoxText").text("Please, provide size");
				$("#MessageBox").show();
            }
            
			validateTypeData(){

				return !isNaN($("#fSize").val());
            }
            
			post(){
				super.post("addDvdDisk.php",
				{
					sku: $("#fSku").val(),
					name: $("#fName").val(),
					price: $("#fPrice").val(),
					size: $("#fSize").val()
					
				});					
				
			}			
		}

		class FurnitureGUI extends GUI {
			showInputFields() {
				$("#lSize").hide();
				$("#lWeight").hide();
				
				$("#lDimensions").show();
				
				$("#MessageBoxText").text("Please, provide dimensions");
				$("#MessageBox").show();
				
            }
            
			validateTypeData(){  
				return !(isNaN($("#fHeight").val()) ||  isNaN($("#fWidth").val()) ||  isNaN($("#fLength").val()));
            }
            
			post(){
				super.post("addFurniture.php",
				{
					sku: $("#fSku").val(),
					name: $("#fName").val(),
					price: $("#fPrice").val(),
					height: $("#fHeight").val(),
					width: $("#fWidth").val(),
					length: $("#fLength").val()
					
				});				
				
			}			
		}
	

		function setAsDvdDisk(){
			$("#fTypeSwitcher").text("Dvd disk");
			GUIinstance = new DvdDiskGUI();
			GUIinstance.showInputFields();
		}
		
		function setAsBook(){
			$("#fTypeSwitcher").text("Book");
			GUIinstance = new BookGUI();
			GUIinstance.showInputFields();
		}
		
		function setAsFurniture(){
			$("#fTypeSwitcher").text("Furniture");
			GUIinstance = new FurnitureGUI();
			GUIinstance.showInputFields();
		}	

		$(document).ready(function(){
			

			$("#fTypeSwitcherMenu a").on('click', function(e) {
			  var selText = $(this).text();

			});

			$('#saveButton').on('click',function(e){

                //we check if any of the fields hasn't been set
				if ((typeof GUIinstance == 'undefined') || ($("input:visible").filter(function() { return $(this).val() === ""; }).length != 0))
				{
					$("#ErrorBoxText").text("Please, submit required data");
					$("#ErrorBox").show();
					return;
                }

                //we check if all fields has valid inputs
				if(isNaN($("#fPrice").val())  || !GUIinstance.validateTypeData())
				{
					$("#ErrorBoxText").text("Please, provide the data of indicated type");
					$("#ErrorBox").show();
					return;
				}
			
				GUIinstance.post();
			  
            });
            
            $('#cancelButton').on('click',function(e){
				window.location.href = "list.php";
            });
		});
	</script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
	
  </body>
</html>